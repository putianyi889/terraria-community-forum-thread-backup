> So I've had this idea in my head since I've heard that there will be conveyor belt in 1.3.1 update. Prior to 1.3.1, I had to use this way too...

# PC - [Showcase] Pumpkin Moon Coin Farm (255 platinum coins in a night)
1.  > So I've had this idea in my head since I've heard that there will be conveyor belt in 1.3.1 update. Prior to 1.3.1, I had to use [this way too complicated farm](https://youtu.be/Y4Q_5xl_wWs) to generate a world record for how much one can earn coins in one Pumpkin Moon. That was the best I could come up with in 1.3, but there was inevitable loss of loots. Thanks to the conveyor belt, now I can collect literally all the loots a player can generate in one Pumpkin Moon, with an optimized way for the Last Prism. Conveyor belt is, to say the least, the best.
    > 
    > Video:
    > 
    > World Download:  
    > [https://drive.google.com/file/d/0B03JKmg4iLVJNFRFbXR2RmFqNFE/view?usp=sharing](https://drive.google.com/file/d/0B03JKmg4iLVJNFRFbXR2RmFqNFE/view?usp=sharing)
    > 
    > Schematics:  
    > ![[​IMG]](http://i.imgur.com/BZDNWbC.png)
    > 
    >   
    > ![[​IMG]](http://i.imgur.com/Sv0nsv4.png)  
    > 
    >   
    > This farm shares the same basic concept with my [Pumpkin/Frost Moon Arena](http://forums.terraria.org/index.php?threads/showcase-1-3-pumpkin-frost-moon-new-records-arena.39793/). Every mob spawns into Last Prism's beam and advance under it, until they die. So if you're careful enough, even Pumpkings never reach you.  
    > Sadly this farm does not work for Frost Moon, since Ice Queen gradually evades the Last Prism's beam by going under the conveyor belt. But this problem is somewhat irrelevant, I made this farm sorely for the purpose of getting a world record for how much one can earn coins in one Pumpkin Moon. Besides Frost Moon is not as efficient as Pumpkin Moon in terms of farming coins.
    > 
    > Procedure
    > 
    > 1.  Make autopause on in the settings, ride Cute Fishron, then teleport into the farm. I found out that this particular way of placing blocks
    >     
    >     made the player immune to knockback while riding Cute Fishron.
    > 2.  Touch Bewitching Table and Crystal Ball, summon 2 Ravens into the crab statue chamber, then flip the top switch. This way you can collect mana stars and boosters indefinitely, which means you can fire the Last Prism continuously with increased damage.
    > 3.  Activate the conveyor belt by flipping the middle switch, then drink potions shortly before 7:30.
    > 4.  Use Pumpkin Moon Medallion at 7:30. Then fire Last Prism to the lower left, along the conveyor belt.
    > 5.  Use quick stack to store your loots into the chests around you. I'd recommend you to do this every 10 seconds, so that loose items never hit the 400 item limit.
    > 6.  After Pumpkin Moon is over, Activate the king statue and sell all the loots to the NPC. Make sure to craft your Spooky Woods into Spooky Helmets and sell them, since it is the most efficient way to convert Spooky Wood into coins.
    
    Last edited: Jul 9, 2016
    
2.  > Hi, it's a nice Farm. Easy to construct and effective.I got a similar idea but instead of letting the enemies fall on a surface (I didn't know that this work properly), I built a shorter conveyor belt. So they spawn directly in spawn region. I cant use the Fishron buff because sometimes a pumpkin can peak through. So my dps is a bit lower than yours but! my items are less likely to disapear.  
    > So far I only got 304k points on my farm with 107platin without selling. After selling all my stuff I got about  
    > 250 platin (36gold missing ![:(](https://forums.terraria.org/styles/default/xenforo/clear.png "Sad    :("))  
    > I think your greedy Ring won't help, it may work even against you. He most likely spawn copper or silver coins. This may despawn a valuable item.
    > 
    > Sadly, a good time for final wave depends hardly on luck (type of enemy and the spawning time seem to be totally random ). An update which fix this would be awesome.  
    > So far my best time is 8:13pm. How often you tried to get your time?
    > 
    >   
    > (beta version, free to optimize it)  
    > In each chest I have one Item from pumpkin event to use quick stack.
    
    Last edited: Jun 5, 2016
    
3.  > 250!? Wow, great job! I withdrew my proclaimed world record. Sorry if I sound rude, but you wouldn't sell items that you stored beforehand to use quick stack, would you?  
    > So which means items do hit 400 limit and disappear in my farm... I didn't notice that until now. I need to try to store my loots every 30 seconds without greedy ring then.  
    > I will try your method also. Thank you!  
    > My best record (and hopefully the world record) is 8:08 pm in [this video](https://youtu.be/q92j5A-B1lM), and if I recall correctly I tried roughly 30 times per day and got this record in day 6. This one (8:10 pm) on the other hand, took only about 40 tries. So yes, it depends sorely on luck, as you said.
    > 
    > By the way, your research on horseman's blade and statues back in 1.2.4 was awesome! It helped me a lot![:)](https://forums.terraria.org/styles/default/xenforo/clear.png "Smile    :)")
    
4.  > That's a good question. I sold them but subtracted the value afterwards. Together the items have a value of less than 4 platin. Before subtraction I had over 250, after only 249p 64g ![:(](https://forums.terraria.org/styles/default/xenforo/clear.png "Sad    :(")Maybe greedy ring work as well with shorter storage intervall. Dont take my words to seriously. I was just an idea.Wow, that's endurance. You are probably close to the optimal time.I'm not an expert, I just had some fun. Now with the 1.3 patch its worthless^^. The honor goes to people like you who upload such awesome yt videos.
    > 
    > Edit: Just to let you know 8:07pm is possible. I tried to get over 250p but I played to risky and died later on ![:(](https://forums.terraria.org/styles/default/xenforo/clear.png "Sad    :(")  
    > Edit2: I did some more trials. My best final wave time ist still 8:07pm (but got it again). With 8:11pm I got 255p and 312.450points.  
    > Edit3: (slightly modified map as attached file)
    
    Last edited: Jun 10, 2016
    
5.  > I'll be building one based on this, thanks, but, if you're using autopause, why not only save the wood and sell the items as you get them? IIRC the NPC shouldn't affect the spawn rate on moon events.  
    > Edit: oh, and if the jungle biome is there for the yo-yo, why not add snow biome too?
    
    Last edited: Jun 8, 2016
    
6.  > Congrats! And yes, I think theoretically even 8:00 pm is possible as I mentioned in [my guide](http://forums.terraria.org/index.php?threads/showcase-1-3-pumpkin-frost-moon-new-records-arena.39793/).You are absolutely right, I had completely forgotten about it. I guess I have to modify this farm...  
    > Do biomes overlap?
    
7.  > Well, when I do key farms I usually place 200 snow + 100<hallow/crimson(200)corruption> ice blocks + 80 jungle. I end up getting too many yoyos.  
    > Another question, since its not entirely necessary for the crabs to be killed faster by the ravens, do you think 1 second timers can do the trick? I don't like using the target ghost (mostly because I don't know how it works, and im worried it will get fixed).  
    > Edit: 100 hallow ice blocks, 200 if corruption or crimson.
    
    Last edited: Jun 9, 2016
    
8.  > In this farm, yes. Your mana won't run out because of the conveyor belt bringing a ton of mana stars to you.
    
9.  > It seems that the enemy only spawns below the usal spawn surface if the slope is on the left side. I couldn't manage to mirror it on the right side.
    
10.  > Really? I didn't know that. It sounds unlikely... I think further research is needed there.
    
11.  > That's odd, did you place the walls?
    
12.  > I mirroed it to the right side.  
    > [![123.jpg](https://forums.terraria.org/data/attachments/117/117027-14b37f1293432bf91c84c905fae0629a.jpg)](https://forums.terraria.org/index.php?attachments/123-jpg.116945/)
    
13.  > Well, idk how to help, by the way, something that maybe could help someone come up with an arena, we already know the enemies spawn falls, usually enemies despawn because they're too far away of the player, but some times flying enemies will stay and go after the player above. (This happened to me a lot on console when I had a big area about 100 blocks down empty, corruption enemies would fly up and attack me)  
    > Maybe there is a way to prevent other than flying enemies to spawn on the moon events, that way we get only the hardest like Ice queen, and get more points.
    
14.  > Thank you for giving me motivation! I upgraded my farm so that I can use quick stack.  
    >   
    > Final wave at 8:06 pm, 255 platinum coins. Total point is 304500, still less than yours. Damn luck...
    
15.  > Oh man I tried making my own, looks like your latest desing, but with a few more chest, the problem is I keep making the same mistake at the last wave, by whatever reason I don't activate the last prism soon enough, so enemies end up above me, and they kill me, also that's why it can't be used for frost moon, im not trying either ever again.....  
    > But nicely done! (Again if using autopause why not just sell the items instead of quick stack?)
    
16.  > ![:redspin:](https://forums.terraria.org/assets/emoticons/red/red.png "Spinning Redigit    :redspin:")  
    > [@terramappy](https://forums.terraria.org/index.php?members/43152/) I'm flabbergasted!!
    > 
    > After watching the video you linked up top saying it wasn't very good, i watched all of your youtube content.
    > 
    > Now more than ever i have to have the 1.3 content!!!! Wonder how long XboxOne will have to wait....
    
    Last edited: Jul 11, 2016
    
17.  > Some nice work! You are not only faster you also got about 42 gold more than me ![:(](https://forums.terraria.org/styles/default/xenforo/clear.png "Sad    :(") and you did it in one run together. Nice one!  
    > if interessted in some more stats I captured: 704 pumkin trophies, 684 mouring wood trophies, 27343 wood, 113p2g without selling, 255p40g after selling, 312.450points  
    > You may got some lucky item drops or I missed to stack up fast enough.
    > 
    > Anyway I won't try again in this patch, congrats to your results. I don't think anyone can get faster than this. Maybe with 1000 trials you can (with some luck) but thats not worth it. Terraria needs a event with points and wihtout luck. If you do the same all the time, the points should be the same all the time.
    

[![putianyi888](https://forums.terraria.org/styles/overworld/xenforo/avatars/avatar_male_m.png)](https://forums.terraria.org/index.php?members/putianyi888.121300/)